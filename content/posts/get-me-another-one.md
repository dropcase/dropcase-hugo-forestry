---
title: "Get Me Another One"
date: 2019-11-03T17:51:20-05:00
draft: false
---

# Trying Another Option

(plus more options on top of that)

So just as fast I as started testing out [Hugo](https://gohugo.io/) on [Netlify CMS](https://www.netlifycms.org/), I found another way to run a CMS behind the scenes. It's with [Forestry](https://forestry.io/), which gives a nice backend to manage content. That piece was something that I felt like I was missing with SSGs.

I love the fast rendering, the option to host through [GitLab](https://www.gitlab.com/) or [GitHub](https://www.github.com/), and focusing on the writing piece as opposed to all of the stuff around it. That's one of the things I used to get stuck in - all of the setup and configuration and add-ons and modules and themes (ad nauseum).

This might not be the long-term path forward but it's what I want to do for now. That's good enough to get started. Next steps are to get some of the content from my other work into here, and get the theme I had on Hexo working (hooray that someone ported it!). It's ever-evolving and might be different in a month.

Hopefully not, and once the dust has settled here I can try to work on some other projects I'm planning. Whew.

UPDATE - almost three years later, I don't think I touched this repo since I wrote this
